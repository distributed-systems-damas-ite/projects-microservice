DROP TABLE IF EXISTS Projects;

CREATE TABLE Projects (
    `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
    `user_id` int,
    `name` varchar(50),
    `description` text
);

insert into Projects (id, user_id, name, description) values (1, 1, 'Lotstring', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.');
insert into Projects (id, user_id, name, description) values (2, 1, 'Flexidy', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.');
insert into Projects (id, user_id, name, description) values (3, 1, 'Kanlam', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.');
insert into Projects (id, user_id, name, description) values (4, 1, 'Y-find', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.');
insert into Projects (id, user_id, name, description) values (5, 1, 'Sonair', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.');
insert into Projects (id, user_id, name, description) values (6, 1, 'Temp', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.');
insert into Projects (id, user_id, name, description) values (7, 1, 'Aerified', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.');
insert into Projects (id, user_id, name, description) values (8, 1, 'Wrapsafe', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.');
insert into Projects (id, user_id, name, description) values (9, 1, 'Zontrax', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.');
insert into Projects (id, user_id, name, description) values (10, 1, 'Temp', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.');
insert into Projects (id, user_id, name, description) values (11, 1, 'Zontrax', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.');
insert into Projects (id, user_id, name, description) values (12, 1, 'Tampflex', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.');
insert into Projects (id, user_id, name, description) values (13, 1, 'Asoka', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.');
insert into Projects (id, user_id, name, description) values (14, 1, 'Fix San', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.');
insert into Projects (id, user_id, name, description) values (15, 1, 'Bigtax', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.');
insert into Projects (id, user_id, name, description) values (16, 1, 'Alphazap', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.');
insert into Projects (id, user_id, name, description) values (17, 1, 'Treeflex', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.');
insert into Projects (id, user_id, name, description) values (18, 1, 'Bigtax', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.');
insert into Projects (id, user_id, name, description) values (19, 1, 'Stronghold', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.');
insert into Projects (id, user_id, name, description) values (20, 1, 'Stronghold', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.');