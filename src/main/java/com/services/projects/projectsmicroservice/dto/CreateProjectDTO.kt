package com.services.projects.projectsmicroservice.dto

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class CreateProjectDTO (
        @field:NotNull(message = "Name is required")
        @field:NotBlank(message = "Name can not be empty")
        var name: String,

        @field:NotNull(message = "Description is required")
        @field:NotBlank(message = "Description can not be empty")
        var description: String?,

        var userId: Int) {

}