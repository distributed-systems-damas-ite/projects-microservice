package com.services.projects.projectsmicroservice.dtos

data class DeleteTasksRequestDto(
        val projectId: Int
)