package com.services.projects.projectsmicroservice.repositories

import com.services.projects.projectsmicroservice.models.Project
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProjectRepository : JpaRepository<Project, Int> {

    fun findByUserId(userId: Int): List<Project>

}