package com.services.projects.projectsmicroservice.services

import com.services.projects.projectsmicroservice.models.Task
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@FeignClient("taskservice",fallback =  TaskServiceFallback::class)
public interface TaskService {

    @GetMapping("/tasks?projectId={id}")
    fun getTasksByProjectId(@PathVariable("id") projectId: Long): List<Task>

}
@Component
class TaskServiceFallback : TaskService{
    override fun getTasksByProjectId(projectId: Long): List<Task> {
        // TODO: make cach and get data from cache if there is any
        // for now return empty list
        return emptyList();
    }

}
