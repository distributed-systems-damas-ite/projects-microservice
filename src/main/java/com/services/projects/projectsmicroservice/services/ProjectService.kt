package com.services.projects.projectsmicroservice.services

import com.services.projects.projectsmicroservice.annotations.RegisterNumberOfExecutions
import com.services.projects.projectsmicroservice.dto.CreateProjectDTO
import com.services.projects.projectsmicroservice.models.Project
import org.springframework.stereotype.Service

@Service
interface ProjectService {

    fun getAllProjects(userId: Int): List<Project>

    fun getProjectById(userId: Int, projectId: Int): Project?

    @RegisterNumberOfExecutions
    fun createProject(userId: Int, project: CreateProjectDTO): Project

    @RegisterNumberOfExecutions
    fun deleteProject(userId: Int, projectId: Int): Boolean

    @RegisterNumberOfExecutions
    fun updateProject(userId: Int, project: Project): Project

}