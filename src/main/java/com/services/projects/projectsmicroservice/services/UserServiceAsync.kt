package com.services.projects.projectsmicroservice.services

import com.services.projects.projectsmicroservice.models.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture

@Service
public open class UserServiceAsync {

    @Autowired
    private lateinit var userService: UserService

    @Async
    open fun findById(id: Int?): CompletableFuture<User> {
        return CompletableFuture.completedFuture(userService.findById(id))
    }

}