package com.services.projects.projectsmicroservice.services

import com.services.projects.projectsmicroservice.models.User
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable


@FeignClient("userservice",fallback =  UserServiceFallback::class)
interface UserService {

    @GetMapping("/users/{id}")
    fun findById(@PathVariable id: Int?): User

}
@Component
class UserServiceFallback : UserService {
    override fun findById(id: Int?): User {
        return User(0, "User", "username")
    }

}
