package com.services.projects.projectsmicroservice.services

import com.services.projects.projectsmicroservice.annotations.RegisterNumberOfExecutions
import com.services.projects.projectsmicroservice.dto.CreateProjectDTO
import com.services.projects.projectsmicroservice.dtos.DeleteTasksRequestDto
import com.services.projects.projectsmicroservice.models.Project
import com.services.projects.projectsmicroservice.models.Task
import com.services.projects.projectsmicroservice.models.User
import com.services.projects.projectsmicroservice.repositories.ProjectRepository
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture

@Service
open class ProjectServiceImpl : ProjectService {

    @Autowired
    private lateinit var projectRepository: ProjectRepository

    @Autowired
    private lateinit var taskService: TaskService

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var taskServiceAsync: TaskServiceAsync

    @Autowired
    private lateinit var userServiceAsync: UserServiceAsync

    @Value("\${rabbitmq.projects.exchange}")
    lateinit var exchange: String

    @Value("\${rabbitmq.projects.routingKey}")
    lateinit var routingKey: String

    @Autowired
    lateinit var rabbitTemplate: RabbitTemplate


    override fun getAllProjects(userId: Int) = projectRepository.findByUserId(userId)


    override fun getProjectById(userId: Int, projectId: Int): Project? {
        val project = getAllProjects(userId).firstOrNull { it.id == projectId }
        project?.let {
            val cf1: CompletableFuture<User> = userServiceAsync.findById(it.userId)
            val cf2: CompletableFuture<List<Task>> = taskServiceAsync.getTasksByProjectId(projectId.toLong())
            CompletableFuture.allOf(cf1, cf2).join()
            it.tasks = cf2.get()
            return it
        }
        return null
    }

    @RegisterNumberOfExecutions
    override fun createProject(userId: Int, project: CreateProjectDTO): Project {
        val mProject = Project()
        mProject.name = project.name
        mProject.description = project.description
        mProject.userId = userId
        return projectRepository.save(mProject)
    }

    @RegisterNumberOfExecutions
    override fun deleteProject(userId: Int, projectId: Int): Boolean {
        getProjectById(userId, projectId)?.let {
            projectRepository.delete(it)
            deleteAllTasks(projectId)
            return true
        }
        return false
    }

    @RegisterNumberOfExecutions
    override fun updateProject(userId: Int, project: Project): Project {
        val mProject = getProjectById(userId, project.id!!)
        mProject!!.name = project.name
        mProject.description = project.description
        return projectRepository.save(mProject)
    }

    private fun deleteAllTasks(projectId: Int) {
        rabbitTemplate.convertAndSend(exchange, routingKey, DeleteTasksRequestDto(projectId))
    }

}