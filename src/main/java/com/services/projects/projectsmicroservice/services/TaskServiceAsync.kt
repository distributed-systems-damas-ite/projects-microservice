package com.services.projects.projectsmicroservice.services

import com.services.projects.projectsmicroservice.models.Task
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture

@Service
public open class TaskServiceAsync {

    @Autowired
    private lateinit var taskService: TaskService

    @Async
    open fun getTasksByProjectId(projectId: Long): CompletableFuture<List<Task>> {
        return CompletableFuture.completedFuture(taskService.getTasksByProjectId(projectId))
    }

}

