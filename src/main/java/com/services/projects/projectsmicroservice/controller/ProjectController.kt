package com.services.projects.projectsmicroservice.controller

import com.services.projects.projectsmicroservice.dto.CreateProjectDTO
import com.services.projects.projectsmicroservice.models.Project
import com.services.projects.projectsmicroservice.services.ProjectService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/projects")
class ProjectController {

    @Autowired
    private lateinit var projectService: ProjectService

    @GetMapping
    fun findAll(@RequestParam userId: Int) = projectService.getAllProjects(userId)

    @GetMapping("/{id}")
    fun findById(@RequestParam userId: Int, @PathVariable id: Int) = projectService.getProjectById(userId, id)

    @PostMapping
    fun create(@RequestParam userId: Int, @Valid @RequestBody project: CreateProjectDTO) = projectService.createProject(userId, project)

    @PutMapping
    fun update(@RequestParam userId: Int, @Valid @RequestBody project: Project) = projectService.updateProject(userId, project)

    @DeleteMapping
    fun delete(@RequestParam userId: Int, @RequestParam projectId: Int) = projectService.deleteProject(userId, projectId)

}