package com.services.projects.projectsmicroservice.annotations

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class RegisterNumberOfExecutions