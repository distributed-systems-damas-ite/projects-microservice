package com.services.projects.projectsmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableFeignClients
@EnableAsync
@EnableCircuitBreaker
public class ProjectsMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectsMicroserviceApplication.class, args);
    }

}
