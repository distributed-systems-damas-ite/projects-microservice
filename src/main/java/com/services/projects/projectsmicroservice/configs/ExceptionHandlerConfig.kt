package com.services.projects.projectsmicroservice.configs

import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.validation.FieldError
import java.util.HashMap
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus


@RestControllerAdvice
class ExceptionHandlerConfig {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = [
        MethodArgumentNotValidException::class
    ])
    fun handleValidationExceptions(ex: MethodArgumentNotValidException): Map<String, String> {
        val errors = HashMap<String, String>()
        ex.bindingResult.allErrors.forEach { error ->
            val fieldName = (error as FieldError).field
            val errorMessage = error.getDefaultMessage()
            errors[fieldName] = errorMessage ?: ""
        }
        return errors
    }

}