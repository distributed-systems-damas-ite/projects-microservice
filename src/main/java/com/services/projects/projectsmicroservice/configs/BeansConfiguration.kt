package com.services.projects.projectsmicroservice.configs

import com.services.projects.projectsmicroservice.services.ProjectService
import com.services.projects.projectsmicroservice.services.ProjectServiceImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class BeansConfiguration {

    @Bean
    open fun projectService(): ProjectService {
        return ProjectServiceImpl()
    }

}