package com.services.projects.projectsmicroservice.configs

import org.springframework.amqp.core.*
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class QueueConfig {

    @Value("\${rabbitmq.projects.queue}")
    lateinit var queueName: String

    @Value("\${rabbitmq.projects.exchange}")
    lateinit var exchange: String

    @Value("\${rabbitmq.projects.routingKey}")
    lateinit var routingKey: String

    @Bean
    open fun myQueue() = Queue(queueName, false)

    @Bean
    open fun exchange() = DirectExchange(exchange)

    @Bean
    open fun binding(queue: Queue, exchange: DirectExchange): Binding = BindingBuilder.bind(queue).to(exchange).with(routingKey)

    @Bean
    open fun jsonMessageConverter() = Jackson2JsonMessageConverter()

    @Bean
    open fun mRabbitTemplate(connectionFactory: ConnectionFactory) = with(RabbitTemplate(connectionFactory)) {
        messageConverter = jsonMessageConverter()
        return@with this
    }

}