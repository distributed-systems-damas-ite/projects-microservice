package com.services.projects.projectsmicroservice.models

import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Entity
@Table(name = "Projects")
data class Project(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        var id: Int?,
        @Column(name = "name")
        @field:NotNull(message = "Name is required")
        @field:NotBlank(message = "Name can not be empty")
        var name: String,
        @Column(name = "description")
        @field:NotNull(message = "Description is required")
        @field:NotBlank(message = "Description can not be empty")
        var description: String?,
        @Column(name = "userId")
        var userId: Int?,
        @Transient
        var tasks: List<Task>
) {

    constructor(project: Project) : this(null, project.name, project.description, null, ArrayList())

    constructor() : this(null, "", null, null, ArrayList())

}