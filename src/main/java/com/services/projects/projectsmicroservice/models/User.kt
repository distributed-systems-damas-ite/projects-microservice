package com.services.projects.projectsmicroservice.models

data class User(
        val id: Long?,
        val name: String?,
        val username: String?
)