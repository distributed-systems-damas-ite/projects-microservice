package com.services.projects.projectsmicroservice.models

data class Task(
        val id: Long?,
        val projectId: Long?,
        val name: String?,
        val description: String?,
        val userEstimation: Float?,
        val aiEstimation: Float?
)