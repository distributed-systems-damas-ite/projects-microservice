package com.services.projects.projectsmicroservice

import io.micrometer.core.instrument.MeterRegistry
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.After
import org.aspectj.lang.annotation.Aspect
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Aspect
@Component
class ExecutionCountAspect {

    @Autowired
    private lateinit var meterRegistry: MeterRegistry

    @After("@annotation(com.services.projects.projectsmicroservice.annotations.RegisterNumberOfExecutions)")
    fun logExecutionTime(joinPoint: JoinPoint) {
        this.meterRegistry.counter(joinPoint.signature.toShortString().split("\\(".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]).increment()
    }

}